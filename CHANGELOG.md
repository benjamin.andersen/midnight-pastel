<div align = "center">

&nbsp;

![Midnight Pastel](https://i.imgur.com/ERNTFNS.png)

&nbsp;

<h1>

Change Log

</h1>

&nbsp;

</div>

~~~
Note: | This theme is an early release. I definitely have missed some
      | things, so please help me improve this theme. Thanks!
~~~

&nbsp;

### (0.3.1 - 0.3.8)&emsp;|&emsp;[10/13/2020 - 08/24/2021]
- Fixed bad color for import/export all
- Added syntax highlighting for Python decorators
- Made highlighting for python string interpolation match string color
- Fixed inconsistent syntax highlighting for python function decorators
- Fixed inconsistent syntax highlighting for python functions
- Fixed issue where python class attributes are highlighted as functions
- Added some semantic syntax highlighting support
- Improved syntax highlighting for C++
- Improved `README`

&nbsp;

### (0.2.1 - 0.3.0)&emsp;|&emsp;[09/14/2020 - 10/12/2020]
- Made `meta.brace.round` and `meta.brace.square` default to White
- Fixed some inconsistent CSS syntax highlighting
- Fixed missing type highlighting for `.`
- Reverted color of import/export variable colors

&nbsp;

### (0.1.5 - 0.2.0)&emsp;|&emsp;[09/09/2020 - 09/14/2020]
- Fixed missing syntax highlighting for some variable scopes
- Made `"` for JSON keys match color
- Made color of 'console' in `console.log` White
- Made import and export variable colors darker
- RGB Hex codes are now Deep Orange
- `!` operator is now Deep Orange
- `=` operator is now White
- `.` operator is now much darker
- Fixed bad syntax highlighting:
    - `Constant.language` scope was not specified
    - HTML entities in Mardown
    - Sass keywords
    - Compound assignments now match operator color
    - CSS entity punctuation defintion now match entity color

- Made `entity.name.tag.reference` Blue
- Made selection background more consistent
- Recommended usage with Fira Code
- Organized `-color-theme.json` better
- Improved `README` and `CHANGELOG`

&nbsp;

### (0.0.2 - 0.1.0)&emsp;|&emsp;[09/07/2020 - 09/08/2020]
- Added syntax highlighting for Markdown
- Decreased diff editor opacity
- Added "invalid.deprecated" scope
- Added "keyword.operator.expression.typeof" scope
- Darkened editor selections so you can see selected comments
- Made booleans Deep Orange instead of Teal for better contrast
- Made embedded js in templates and JSX Deep Orange instead of Teal
- Added MIT License
- Improved `README` and `CHANGELOG`

&nbsp;

### (0.0.1)&emsp;|&emsp;[09/06/2020]
- Initial release