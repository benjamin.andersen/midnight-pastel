<div align = "center">

&nbsp;

![Midnight Pastel](https://i.imgur.com/ERNTFNS.png)

&nbsp;

<h1>

Midnight Pastel

</h1>
</div>

&nbsp;

~~~
Note: | This theme is an early release. I definitely have missed some
      | things, so please help me improve this theme. Thanks!
~~~

<div align = "center">

&nbsp;

*An easy on the eyes, high-contrast dark theme for VSCode with vibrant, pastel colors.*

*Inspired by, and tweaked from [Darker High Contrast Material Theme](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-community-material-theme).*

*Recommended usage with [Fira Code](https://github.com/tonsky/FiraCode) and [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme).*


&nbsp;

<h1>

Sample Code

</h1>

&nbsp;

### Javascript

&nbsp;

![Javascript](https://i.imgur.com/Wuc2fmg.png)

&nbsp;

### JSON

&nbsp;

![JSON](https://i.imgur.com/C5wG3RS.png)

&nbsp;

### CSS

&nbsp;

![CSS](https://i.imgur.com/UNLZdim.png)

&nbsp;

### Markdown

&nbsp;

![Markdown](https://i.imgur.com/P9NFGMW.png)

</div>